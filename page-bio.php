<?php
/*
Template Name: Team
*/
?>

<?php get_header(); ?>
		<?php the_post();

// Get 'team' posts
$team_posts = get_posts( array(
	'post_type' => 'team',
	'posts_per_page' => -1, // Unlimited posts
	'orderby' => 'title', // Order alphabetically by name
) );

if ( $team_posts ):
?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage" style="padding-top:1em;">
							
	

<header class="page-header biography">

<div class="page-header__text">
<h1 class="page-header__title"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/heart__3c.svg" width="3%" ><?php the_title(); ?></h1>
<p class="page-header__subtitle"><?php the_field('subtitle'); ?></p>

</div>

</header>
	<article class="bio--container">
<?php 
	foreach ( $team_posts as $post ): 
	setup_postdata($post);
	

	?>





<div class="team__container">
<div style=" max-width: 600px;">
<a href="#<?php global $post; echo $post->post_name; ?>" rel="modal:open">

<div class="team__overlay"></div>
	<div class="team__photo" style="background:url('<?php the_field('practitioner__photo')?>');">
			</div>
	<div class="team__title">
			<h3 ><?php the_title(); ?></h3>
			<p class="lead position"><?php the_field('team_position'); ?><?php the_field('titles'); ?></p>
			
</div></a>
</div>
		</div>



<div id="<?php global $post; echo $post->post_name; ?>" style="display:none">

	<div>
		<h1><?php the_title(); ?></h1>
		<h3><?php the_field('team_position'); ?><?php the_field('titles'); ?></h3>
		<p><?php the_field('biography'); ?></p>	
		
	</div>
</div>



	<?php endforeach; ?>

<?php endif; ?>
</article>




<?php get_footer(); ?>
