<?php
// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php'); 

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php'); 
require_once(get_template_directory().'/assets/functions/menu-walkers.php'); 

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php'); 

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php'); 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php'); 

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php'); 

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 

// Use this as a template for custom post types
// require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php'); 

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php'); 

function page_bodyclass() {  // add class to <body> tag
	global $wp_query;
	$page = '';
	if (is_front_page() ) {
    	   $page = 'home';
	} elseif (is_page()) {
   	   $page = $wp_query->query_vars["pagename"];
	}
	if ($page)
		echo 'class= "'. $page. '"';
}


add_action( 'init', 'cptui_register_my_cpts_service' );
function cptui_register_my_cpts_service() {
	$labels = array(
		"name" => __( 'Services', '' ),
		"singular_name" => __( 'Service', '' ),
		"add_new_item" => __( 'Add New Service', '' ),
		"edit_item" => __( 'Edit Service', '' ),
		"new_item" => __( 'New Service', '' ),
		);

	$args = array(
		"label" => __( 'Services', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
				"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "service", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-clipboard",
		"supports" => array( "title", "editor" ),		
		"taxonomies" => array( "category" ),
			);
	register_post_type( "service", $args );

// End of cptui_register_my_cpts_service()
}


add_action( 'init', 'cptui_register_my_cpts_team' );
function cptui_register_my_cpts_team() {
	$labels = array(
		"name" => __( 'Team Members', '' ),
		"singular_name" => __( 'Team Member', '' ),
		"menu_name" => __( 'Team Members', '' ),
		);

	$args = array(
		"label" => __( 'Team Members', '' ),
		"labels" => $labels,
		"description" => "Team Members",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
				"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "team", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-groups",
		"supports" => array( "title" ),					);
	register_post_type( "team", $args );

// End of cptui_register_my_cpts_team()
}

add_action( 'init', 'cptui_register_my_taxes_staff_members' );
function cptui_register_my_taxes_staff_members() {
	$labels = array(
		"name" => __( 'Staff Members', '' ),
		"singular_name" => __( 'Staff Member', '' ),
		);

	$args = array(
		"label" => __( 'Staff Members', '' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Staff Members",
		"show_ui" => true,
		"show_in_menu" => false,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'staff_members', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "staff_members", array( "team" ), $args );

// End cptui_register_my_taxes_staff_members()
}?>