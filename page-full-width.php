<?php
/*
Template Name: Full Width
*/
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="full-width__content" class="full-width__content">
			
				    <div id="main"   role="main">
					
						<?php get_template_part( 'parts/loop', 'pagefull' ); ?>
					    					
    				</div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
