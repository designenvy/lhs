<?php	/** Template Name: Front Page */
		get_header(); ?>
		<div class="hero--homepage__container">
			<div class="hero--homepage">
				<div><img class="hero__seal"src="<?php echo home_url(); ?>/wp-content/themes/Carillon/assets/img/hero-seal.svg"></div>
				<div class="hero__headline">
				<h2 class="hero__title">Celebrating 40 Years as A Lubbock Legacy</h2>
				<p class="hero__paragraph">For more than 40 years, Carillon has enjoyed a reputation as the most comprehensive retirement community in West Texas offering financial peace-of-mind with the only true <em>Life</em>Care program in the South Plains.</p>
</div>
</div>
		</div>
		<main class="main__homepage" role="main">

				<article class="main__article">
					<a class="article__image-container--retirement" href="<?php echo home_url(); ?>/benefits-of-carillon/">
						
					</a>
					<h2 class="text__title--blue ">Retirement Living</h2>
					<p>Carillon <em>Life</em>Care Community is an
active, friendly neighborhood, where
you’ll find warm, welcoming neighbors.
We are passionate about West Texas
hospitality, the Texas Tech Red Raiders,
good friends and food, and enjoying
retirement to its fullest. At Carillon,
retirement never looked so good.</p>
					<a href="<?php echo home_url(); ?>/independent-living/" class="text__link--seafoam">Read More&#8230;</a>
				</article>
				<article class="main__article">
					<a class="article__image-container--care" href="<?php echo home_url(); ?>/skilled-care/">
						
					</a>
					<h2 class="text__title--seafoam">Levels of Care</h2>
					<p>Carillon is the only community in West
Texas with a full continuum of on-site
health services offered to residents in
residential living, assisted living or full
nursing care. In the event of a health
challenge, the community you’ve come
to know as “home” provides quality,
resident-centered care.</p>
					<a href="<?php echo home_url(); ?>/levels-of-care/" class="text__link--blue">Read More&#8230;</a>
				</article>
				<article class="main__article">
					<a class="article__image-container--life" href="<?php echo home_url(); ?>/lifecare/">
						
					</a>
					<h2 class="text__title--orange"><em>Life</em>Care</h2>
					<p>Carillon offers the only true <em>Life</em>Care
program in all of West Texas. As the
most comprehensive plan available in
retirement living, <em>Life</em>Care residents are
eligible for unlimited nursing care for
life at predictable costs, providing emotional
and financial peace of mind for
individuals, spouses and their families.</p>
					<a href="<?php echo home_url(); ?>/lifecare/" class="text__link--orange">Read More&#8230;</a>
				</article>



		</main>


		
		<div class="missionbar">
		<ul><li>Independent Living</li> 
		<li>Assisted Living</li>
		<li>Accelerated Rehab</li><br>
		<li>Skilled Care</li>
		<li>Memory Care</li>
	</ul>
		</div>

<div id="quick-contact">
		<?php echo do_shortcode( '[contact-form-7 id="1781" title="quickcontact"]' ); ?>
	</div>
			<div class="livingguide">
	<div class"livingguide__download"><a href="http://www.carillonlubbock.com/?smd_process_download=1&download_id=2196"><h4>Download your free Retirement Living Guide!</h4></a>
	</div>
</div>
<?php get_footer(); ?>