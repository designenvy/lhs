<?php get_header(); ?>
			
			<div id="content">
		

				    <div id="main" class="blog__page" role="main">
						 
					  <!-- To see additional archive styles, visit the /parts directory -->
					    <?php get_template_part( 'parts/loop', 'archive' ); ?>
								
				    </div> <!-- end #main -->

		
				    

    
			</div> <!-- end #content -->

<?php get_footer(); ?>