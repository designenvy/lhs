<?php	/** Template Name: Front Page */
		get_header(); ?>

<div class="page-header">
<div class="slogan">
<span class="subtitle"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/heart.svg" width="3%"> Our Mission</span>
<h1>The best cardiac, thoracic, and vascular surgical care available anywhere.</h1>
</div>

<div class="patient">

<a href="http://lubbockheartsurgery.com/wp-content/uploads/2017/05/PatientForms.pdf" target=_blank"  class="button">New Patient Forms</a>
<a href="https://cernerhealth.com/oauth/authenticate?redirect_uri=https%3A%2F%2Fcernerhealth.com%2Fsaml%2Fsso%2Fresponse%3Fmessage_id%3D_789c8c71-6562-4bf2-aa10-0cb8164c593e%26issuer%3Dhttps%253A%252F%252Fmycare.iqhealth.com%252Fsession-api%252Fprotocol%252Fsaml2%252Fmetadata&sign_in_only=on&client_id=8dd42a71598d4a77b5f9483a6f5c9b2c"  target="_blank" class="button">MyTeamCare</a>
<a href="http://www.umchealthsystem.com/patients-visitors/patient-financial-services/bill-pay" target="_blank" class="button">Pay Your Bill</a>

</div>
</div>

<div class="cta__icons">

<div class="cta__module">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/chart.svg">
<h2>Pre-Surgery <br>Questions?</h2>
<p>What to expect, procedures and pre-sugery preparations. </p>
<a href="http://www.lubbockheartsurgery.com/services/" class="cta-button">F.A.Q</a>
</div>

<div class="cta__module">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar.svg" >
<h2>Regional Clinic Schedule</h2>
<p>Monthly regional clinics in Texas and New Mexico. </p>
<a href="<?php echo home_url(); ?>/outreach-clinics/" class="cta-button">Check Dates</a>

</div>

<div class="cta__module">
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/hospital.svg" >
<h2>Office Info</h2>
<h5>Monday – Friday</h5>
<data itemprop="openingHours" value="Mo-Fr 08:00-5:00">8:00 a.m. – 5:00 p.m.</data>
<h5>Phone</h5>
				<a href="tel:8006589507">800.658.9507</a>
				<a href="tel:8067928185">806.792.8185</a>



</div>

</div>
<?php get_footer(); ?>