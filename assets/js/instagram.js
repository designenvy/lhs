$(function() {
    $.ajax({
        type: "GET",
        dataType: "jsonp",
        cache: false,
        url: "https://api.instagram.com/v1/users/324578616/media/recent/?access_token=324578616.a590b7d.a3e93ad894764e858878960293a4dfba",
        success: function(data) {
            for (var i = 0; i < 18; i++) {
                $(".instagram-wrapper").append("<li><a class='gallery-instagram' href='" + data.data[i].images.standard_resolution.url +"' title='Instagram'></a></li>");
            }
        }
    });
});
$('.gallery-instagram').click(function(e) {
    e.preventDefault();
    $(".gallery-instagram").colorbox({rel:'gallery-instagram', transition:"none",width:"960", height:"90%", slideshow:true});
});