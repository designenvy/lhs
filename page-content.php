<?php
/*
Template Name: Text Heavy Page
*/
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content" class="inner-content">
			
				    <div id="main"   role="main">
					
						<?php get_template_part( 'parts/loop', 'pagecontent' ); ?>
					    					
    				</div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
