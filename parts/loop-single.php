<article id="post-<?php the_ID(); ?> "   class="single__blog" role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
	<header class="article-header">	
		<h1  class="blog__title" itemprop="headline"><?php the_title(); ?></h1>
		
    </header> <!-- end article header -->
					
    <section class="entry-content" itemprop="articleBody">
		<div class="blog__photo"><?php the_post_thumbnail('medium'); ?></div>
		<div class="blog__info"><?php the_content(); ?></div>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		 <p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>	</footer> 
		<!-- end article footer -->
									

													
</article> <!-- end article -->