<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
							
 <!-- end article header -->
						
	    <section class="entry-content" itemprop="articleBody">

<article class="service__content">

		    <?php the_content(); ?>

		    <?php wp_link_pages(); ?>
		</article>
		</section> <!-- end article section -->
							
		<footer class="article-footer">
			
		</footer> <!-- end article footer -->
							    

						
	</article> <!-- end article -->
	
<?php endwhile; endif; ?>							