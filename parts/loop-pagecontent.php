<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
							
	

<header class="page-header">
<div class="page-header__bg" style="background-image:url('<?php the_field('photo_header'); ?>');">
</div>
<div class="page-header__text">
<h1 class="page-header__title"><img src="<?php echo home_url(); ?>/wp-content/themes/LHS/assets/images/heart__3c.svg" width="3%" ><?php the_title(); ?></h1>
<p class="page-header__subtitle"><?php the_field('subtitle'); ?></p>
</div>
</header>











<?php

// vars	
$page__cta = get_field('page__cta');


// check
if( $page__cta && in_array('Page CTA', $page__cta) ): ?>

<div class="button__cta">
<a class="button__choose forms" href="#">
<?php the_field('cta__button1'); ?>
</a>
<a class="button__choose bill" href="#">
<?php the_field('cta__button2'); ?>
</a>
		</div>
<?php endif; ?>



		<!-- end article header -->
						
	    <section class="entry-content" itemprop="articleBody">

<article class="fullpage__article ">
			  
		    <?php the_content(); ?>
		    <?php wp_link_pages(); ?>
		</article>
		</section> <!-- end article section -->
							

							    

						
	</article> <!-- end article -->
	
<?php endwhile; endif; ?>							