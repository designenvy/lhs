<article id="post-<?php the_ID(); ?> "   class="single__blog" role="article" itemscope itemtype="http://schema.org/BlogPosting">
						

					<header class="page-header">
<div class="page-header__services">
<div class="page-header__text">
<h1 class="service-header__title"><?php the_title(); ?></h1>



</div>
</div>

</header>	
    <section class="entry-content" itemprop="articleBody">

		<div class="service__info">



<section class="to-expect">
<div class="expect__title">

<h3> What To Expect</h3>
</div>
<div class="expect__border"></div>
<div class="expect__summary">
	<?php the_content(); ?>
	</div>
</section>


<section class="surgery">
<div class="surgery__before">
<h4>Before </h4>
<ul>
<li><h5>For All Surgeries</h5><ul>
<li>Please call our office at 4:30pm the day prior to your procedure to obtain your arrival time for the nexdt day. For Monday procedures, please call on the Friday prior to your procedure.</li>
</ul>
</li>
<li><h5>UMC</h5>
<ul>
<li>You will have a pre-op appointment in outpatient services that will include preop teaching as well as lab work</li></ul>
</li>
<li><h5>LHH or CMC</h5>
<ul>
<li>You will arrive at your respective hospital for pre-op teaching &amp; lab work as determined by our office. Please call our office to obtain that time</li>
</ul>
</li>


</ul>
</div>
<div class="surgery__after">
<h4>After</h4>

<?php the_field('after_surgery'); ?>

</section>


	</div>
	</section> <!-- end article section -->
						
	
		<!-- end article footer -->
									

													
</article> <!-- end article -->